The whole Pacific area is too huge to cover in 1 uncomplicated write-up, so we've preferred a couple of the best locations to stay.
Where is it?

For the reasons of this guide, the Pacific indicates Australia, Micronesia, Fiji and Hawaii. You can find thousands of islands dotted around this area of the Pacific and the Coral Sea, and numerous are largely uncharted by tourists.
Where can I stay?

Every of these countries are used to seeing the sights and Australia and Fiji in specific aren't short of places to live; from impressive hotels to traveler hostels. Europeans travelling to this part of the earth usually stay for some time since of the long flights and since there is so much to discover. Take the weight off your tourism feet and choose to stay in 1 of the growing amount of boutique hotels. Particularly considered for those that like to stay someplace diverse on their travels, these hotels pay alert note to design, detail and service, making sure that your stay is peaceful and simple to ensure that you are able to focus on the city centers, landscapes and actions nearby. In Australia, try the Tower Lodge inside the country's Hunter Valley - internationally famous for its wine creation. With just twelve rooms which are just and stylishly furnish and shaped for most comfort at all the weather, a stop at Tower Lodge is the perfect start to an Australian tour with http://www.paradises.com. 
By contrast, try the lavishness yachts that manage out of French Polynesia. A six or seven night cruise approximately these beautiful islands is superior by the amount of large bedrooms - just 30 - and the adding of many sundeck levels, champagne breakfasts and on-land tour formed to allow you to see the surprise of these islands at 1st hand.
What can I see?

You cannot perhaps fit the whole thing that this area has to give into a pair, but all of the countries of the Pacific advantage from amazing coastlines, memorable mountain, desert and woody areas, rich society and a daring spirit. Apart from of whether you're bungee-jumping in New Zealand, surfing in Australia, river kayaking in French Polynesia or enjoy the scuba diving in Fiji, you're positive to want to go back to the Pacific again to explore its pleasures.
How do I get around?

Transport varies from country to country. Australia and New Zealand are well served by airlines, so it is probable to go to together countries in one trip, just move among New Zealand's North and South islands, or get in as a lot of Australia as you can. It's simple to connect cars in both countries and they work public convey networks that are plainly cheaper and more usual in the cities than in rural areas. 
In Fiji, it is practical to island hop by plane or, more cheaply and slowly, by ferry. Alternatively, you can discover sensible bus services on the superior islands. French Polynesia is a lot the specific same, as is numerous of the other island groups in this branch of the Pacific.
As a vast deal more and a big deal more boutique hotels appear to accommodate travelers who be grateful for design and service, the Pacific islands will attribute in many much more travel wish lists.
